package com.example.tests;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by tomasz on 6/13/2016.
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class SearchExportedFromIDETest {
    private WebDriver driver;
    private String baseUrl = "";
    private StringBuffer verificationErrors = new StringBuffer();
//
//    @Before
//    public void setUp() throws Exception {
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\tomasz\\development\\selenium\\src\\main\\resources\\chromedriver.exe");
//        driver = new ChromeDriver();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//    }

    private static ChromeDriverService service;

    @BeforeClass
    public static void createAndStartService() throws IOException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\tomasz\\development\\selenium\\src\\main\\resources\\chromedriver.exe");
        service = ChromeDriverService.createDefaultService();
        service.start();
    }

    @AfterClass
    public static void createAndStopService() {
        service.stop();
    }


    @Before
    public void createDriver() {
        driver = new RemoteWebDriver(service.getUrl(),
                DesiredCapabilities.chrome());
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void quitDriver() {
        driver.quit();
    }

    @Test
    public void testGoogleSearch() {
        driver.get("http://www.google.com");
        // rest of the test...
    }

//    @Test
//    public void testSearchExportedFromIDE() throws Exception {
//        driver.get("/");
//        driver.findElement(By.id("sted")).clear();
//        driver.findElement(By.id("sted")).sendKeys("Stockholm");
//        driver.findElement(By.id("queryknapp")).click();
//        driver.findElement(By.xpath("//div[@id='directories']/table/tbody/tr/td[2]/a")).click();
//        driver.findElement(By.cssSelector("li")).click();
//        assertEquals("Weather forecast for Stockholm (Sweden)", driver.findElement(By.cssSelector("h1")).getText());
//    }

//    @After
//    public void tearDown() throws Exception {
//        driver.quit();
//        String verificationErrorString = verificationErrors.toString();
//        if (!"".equals(verificationErrorString)) {
//            fail(verificationErrorString);
//        }
//    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}